package;

import ironFetus.Color;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Sprite;
import ironFetus.Vector2;


class BunnySprite extends Sprite
{
    public var pos:Vector2;
	public var width:Int;
	public var height:Int;
	public var angle:Float;
    public var speed:Vector2;
    public var angleSpeed:Float;

    public override function new():Void
    {
        super();

		pos = new Vector2(Fe.random.float(0, Fe.game.width),
            Fe.random.float(0, Fe.game.height));
		width = 26;
		height = 37;

		angle = Fe.random.float(0, 6.28);

        speed = new Vector2(1 + Fe.random.float(0, 5), 1 +
            Fe.random.float(0, 5));
        angleSpeed = Fe.random.float(0, 0.05);

        graphics[0] = new Graphic();
        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;
        graphics[0].angle = angle;
        Graphic.makeQuadVertices(graphics[0], 0, 0, width, height);
        Graphic.makeQuadTexels(graphics[0], 0, 0, 1, 1);
    }

    public override function create():Void
    {
        super.create();
    }

    public override function destroy():Void
    {
        super.destroy();
    }

	public override function preUpdate(delta:Float):Void
    {
        super.preUpdate(delta);
    }

	public override function update(delta:Float):Void
	{
		super.update(delta);

		pos.x += speed.x;
        pos.y += speed.y;

        if (pos.x < 0 || pos.x > Fe.game.width - width)
        {
            speed.x *= -1;
        }
        if (pos.y < 0 || pos.y > Fe.game.height - height)
        {
            speed.y *= -1;
        }

        angle += angleSpeed;

        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;
        graphics[0].angle = angle;
	}

	public override function postUpdate(delta:Float):Void
    {
        super.postUpdate(delta);
    }

    public override function dispose():Void
    {
        super.dispose();
    }
}
