package;

import ironFetus.Camera;
import ironFetus.Debug;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Scene;
import ironFetus.Sprite;
import ironFetus.assets.Assets;
import ironFetus.input.Key;
import ironFetus.input.MouseButton;
import ironFetus.rendering.BasicRenderer;

import BunnySprite;


class BunnyScene extends Scene
{
    public var renderer:BasicRenderer;
    public var numBunnies:Int;

#if (desktop || web)
    public var leftMouseButton:MouseButton;
    public var middleMouseButton:MouseButton;
    public var rightMouseButton:MouseButton;
#end

    public override function init():Void
    {
        backgroundColor = 0xFFFFFFFF;

        Assets.loadImage("bunny", "assets/bunny.png");

        renderer = new BasicRenderer("bunny", [TRANSFORM]);

        renderers.push(renderer);

        cameras.push(new Camera());

        numBunnies = 1000;
        for (i in 0...numBunnies)
        {
            sprites.push(new BunnySprite());
        }
        renderer.addGraphics([for (sprite in sprites) sprite.graphics[0]]);

#if (desktop || web)
        leftMouseButton = Fe.input.mouse.addButton(MouseButton.LEFT);
        leftMouseButton.onDown.push(function (x, y) {
            addBunnies();
        });
#end
#if mobile
        Fe.input.touches.onDown.push(function (x, y) {
            addBunnies();
        });
#end

        var debugKey:Key = Fe.input.keyboard.addKey(Key.SPACE);
        debugKey.onDown.push(function () {
            trace("Bunnies: " + numBunnies + " | " +
                "FPS: " + Debug.fps.current + " | " +
                "Memory: " + Debug.memory + "MB" + " | " +
                "Peak Memory: " + Debug.peakMemory + "MB");
        });

        var restartKey:Key = Fe.input.keyboard.addKey(Key.R);
        restartKey.onDown.push(function () {
            Fe.game.restart();
        });

        var exitKey:Key = Fe.input.keyboard.addKey(Key.ESCAPE);
        exitKey.onDown.push(function () {
            Fe.game.exit();
        });
    }

    public override function update(delta:Float):Void
    {
        super.update(delta);
    }

    public function addBunnies():Void
    {
        var sprite:BunnySprite;
        var graphics:Array<Graphic> = [];
        numBunnies += 500;
        for (i in 0...500)
        {
            sprite = new BunnySprite();
            sprites.push(sprite);
            graphics.push(sprite.graphics[0]);
        }
        renderer.addGraphics(graphics);
    }
}
