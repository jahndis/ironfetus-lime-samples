package;

import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Sprite;
import ironFetus.Vector2;
import ironFetus.input.Key;
import ironFetus.input.MouseButton;


class TestControlSprite extends Sprite
{
    public var pos:Vector2;
    public var speed:Vector2;

    private var rightKey:Key;
    private var leftKey:Key;
    private var downKey:Key;
    private var upKey:Key;

    private var leftMouseButton:MouseButton;

    public override function new():Void
    {
        super();

		pos = new Vector2(Fe.game.width / 2, Fe.game.height / 2 + 100);
        speed = new Vector2(0, 0);

        graphics[0] = new Graphic();

        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;

        graphics[0].vertices = [
            new Vector2(-50, -50),
            new Vector2(50, -50),
            new Vector2(-50, 50),

            new Vector2(50, -50),
            new Vector2(50, 50),
            new Vector2(-50, 50)
        ];

        graphics[0].texels = [
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),

            new Vector2(1, 0),
            new Vector2(1, 1),
            new Vector2(0, 1)
        ];

        rightKey = Fe.input.keyboard.addKey(Key.RIGHT);
        leftKey = Fe.input.keyboard.addKey(Key.LEFT);
        downKey = Fe.input.keyboard.addKey(Key.DOWN);
        upKey = Fe.input.keyboard.addKey(Key.UP);

        leftMouseButton = Fe.input.mouse.addButton(MouseButton.LEFT);
    }

    public override function create():Void
    {
        super.create();
    }

    public override function destroy():Void
    {
        super.destroy();
    }

	public override function preUpdate(delta:Float):Void
    {
        super.preUpdate(delta);
    }

	public override function update(delta:Float):Void
	{
		super.update(delta);

        speed.x = 0;
        speed.y = 0;

        if (rightKey.down)
        {
            speed.x += 5;
        }
        if (leftKey.down)
        {
            speed.x -= 5;
        }
        if (downKey.down)
        {
            speed.y += 5;
        }
        if (upKey.down)
        {
            speed.y -= 5;
        }

#if (desktop || web)
        if (leftMouseButton.down)
        {
            if (Fe.input.mouse.pos.x > pos.x)
            {
                speed.x += 5;
            }
            if (Fe.input.mouse.pos.x < pos.x)
            {
                speed.x -= 5;
            }
            if (Fe.input.mouse.pos.y > pos.y)
            {
                speed.y += 5;
            }
            if (Fe.input.mouse.pos.y < pos.y)
            {
                speed.y -= 5;
            }
        }
#end
#if mobile
        if (Fe.input.touches.down)
        {
            for (touch in Fe.input.touches)
            {
                if (touch.pos.x > pos.x)
                {
                    speed.x += 5;
                }
                if (touch.pos.x < pos.x)
                {
                    speed.x -= 5;
                }
                if (touch.pos.y > pos.y)
                {
                    speed.y += 5;
                }
                if (touch.pos.y < pos.y)
                {
                    speed.y -= 5;
                }
            }
        }
#end

		pos.x += speed.x;
        pos.y += speed.y;

        if (pos.x < 0)
        {
            pos.x = 0;
        }
        if (pos.x > Fe.game.width)
        {
            pos.x = Fe.game.width;
        }
        if (pos.y < 0)
        {
            pos.y = 0;
        }
        if (pos.y > Fe.game.height)
        {
            pos.y = Fe.game.height;
        }
	}

	public override function postUpdate(delta:Float):Void
    {
        super.postUpdate(delta);

        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;
    }

    public override function dispose():Void
    {
        super.dispose();
    }
}
