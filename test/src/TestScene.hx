package;

import ironFetus.Camera;
import ironFetus.Debug;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Save;
import ironFetus.Scene;
import ironFetus.Sound;
import ironFetus.Sprite;
import ironFetus.assets.Assets;
import ironFetus.input.Key;
import ironFetus.input.MouseButton;
import ironFetus.rendering.BasicRenderer;
import ironFetus.text.BitmapFont;
import ironFetus.text.Text;
import ironFetus.text.TextAlign;
import ironFetus.text.TextAnchor;

import TestSprite;
import TestAnimatedTile;
import TestTile;


class TestScene extends Scene
{
    public var tileRenderer:BasicRenderer;
    public var animatedTileRenderer:BasicRenderer;
    public var animatedColorableTileRenderer:BasicRenderer;
    public var textRenderer:BasicRenderer;
    public var simpleSpriteRenderer:BasicRenderer;
    public var spriteRenderer:BasicRenderer;

    public var camera1:Camera;
    public var camera2:Camera;
    public var cameraSpeed:Float;

    public var font:BitmapFont;
    public var text:Text;

    public var music:Sound;
    public var sound:Sound;

#if (desktop || web)
    public var leftMouseButton:MouseButton;
    public var middleMouseButton:MouseButton;
    public var rightMouseButton:MouseButton;
#end

    public override function init():Void
    {
        //var save = new Save("test", "test/path");
        var save = new Save("test");
        save.clear();

        //save = new Save("test", "test/path");
        save = new Save("test");
        trace("Loaded data", save.data);
        save.data.set("test", "derp");
        save.save(function (error) {
            if (error != null)
            {
                trace(error);
                Fe.game.exit();
            }
            trace("Saved data", save.data);
        });

        //save = new Save("test", "test/path");
        save = new Save("test");
        trace("Loaded data", save.data);
        save.data.set("test", "merp");
        save.save(function (error) {
            if (error != null)
            {
                trace(error);
                Fe.game.exit();
            }
            trace("Saved data", save.data);
        });

        backgroundColor = 0xFFFFFFFF;

        Assets.loadImage("image1", "assets/lime.png");
        Assets.loadImage("image2", "assets/lime2.png");
        Assets.loadImage("font", "assets/font.png");
        Assets.loadFont("font", "assets/font.fnt");
        Assets.loadSound("sound", "assets/sound.wav");
        Assets.loadSound("music", "assets/music.ogg");

        tileRenderer = new BasicRenderer("image1");
        animatedTileRenderer = new BasicRenderer("image1", [TEXELS]);
        animatedColorableTileRenderer = new BasicRenderer("image1",
            [TEXELS, COLOR]);
        textRenderer = new BasicRenderer("font", [TRANSFORM, TEXELS, COLOR]);
        simpleSpriteRenderer = new BasicRenderer("image2", [TRANSFORM]);
        spriteRenderer = new BasicRenderer("image2",
            [TRANSFORM, TEXELS, COLOR]);

        spriteRenderer.sortFunction = function (a:Graphic, b:Graphic):Int {
            return a.pos.y > b.pos.y ? 1 : a.pos.y < b.pos.y ? -1 : 0;
        };

        renderers.push(tileRenderer);
        renderers.push(animatedTileRenderer);
        renderers.push(animatedColorableTileRenderer);
        renderers.push(textRenderer);
        renderers.push(simpleSpriteRenderer);
        renderers.push(spriteRenderer);

        camera1 = new Camera();
        /*
        camera1.port.x = 0;
        camera1.port.y = 0;
        camera1.port.width = Fe.game.viewport.width;
        camera1.port.height = Fe.game.viewport.height / 2;
        //*/
        /*
        camera2 = new Camera();
        camera2.port.x = 0;
        camera2.port.y = Fe.game.viewport.height / 2;
        camera2.port.width = Fe.game.viewport.width;
        camera2.port.height = Fe.game.viewport.height / 2;
        //*/
        cameraSpeed = 2;

        cameras.push(camera1);
        //cameras.push(camera2);

        var sprite:Sprite;
        var graphics:Array<Graphic>;

        //*
        var graphic:Graphic;
        graphics = [];
        for (i in 0...10)
        {
            sprite = new TestTile();
            for (graphic in sprite.graphics)
            {
                graphics.push(graphic);
            }
            sprites.push(sprite);
        }
        tileRenderer.addGraphics(graphics);
        //*/

        //*
        graphics = [];
        for (i in 0...10)
        {
            sprite = new TestAnimatedTile();
            for (graphic in sprite.graphics)
            {
                graphics.push(graphic);
            }
            sprites.push(sprite);
        }
        animatedTileRenderer.addGraphics(graphics);
        //*/

        //*
        graphics = [];
        for (i in 0...10)
        {
            sprite = new TestTile();
            for (graphic in sprite.graphics)
            {
                graphics.push(graphic);
            }
            sprites.push(sprite);
        }
        animatedColorableTileRenderer.addGraphics(graphics);
        //*/

        //*
        font = new BitmapFont("font");
        text = new Text(68, font);
        text.text = "ABCDEFGHIJKLM\nNOPQRSTUVWXYZ\n" + 
            "abcdefghijklm\nnopqrstuvxyz\n1234567890";
        text.pos.setTo(Fe.game.width / 2, Fe.game.height / 2);
        text.align = TextAlign.CENTER;
        text.anchor = TextAnchor.CENTER_BOTTOM;
        text.scale.setTo(0.5, 0.5);
        text.color = 0xFFFF0000;
        text.updateGlyphs();
        textRenderer.addGraphics(text.glyphGraphics);
        //*/

        //*
        graphics = [];
        for (i in 0...10)
        {
            sprite = new TestSprite();
            for (graphic in sprite.graphics)
            {
                graphics.push(graphic);
            }
            sprites.push(sprite);
        }
        //*/

        //*
        var controlSprite = new TestControlSprite();
        for (graphic in controlSprite.graphics)
        {
            graphics.push(graphic);
        }
        sprites.push(controlSprite);

        spriteRenderer.addGraphics(graphics);
        //*/

        sound = new Sound("sound");
        music = new Sound("music");
        music.play(true);

#if (desktop || web)
        leftMouseButton = Fe.input.mouse.addButton(MouseButton.LEFT);
        leftMouseButton.onDown.push(function (x, y) {
            trace("Left Down:", x, y);
            sound.play();
        });
        leftMouseButton.onUp.push(function (x, y) {
            trace("Left Up:", x, y);
        });
        middleMouseButton = Fe.input.mouse.addButton(MouseButton.MIDDLE);
        middleMouseButton.onDown.push(function (x, y) {
            trace("Middle Down:", x, y);
        });
        middleMouseButton.onUp.push(function (x, y) {
            trace("Middle Up:", x, y);
        });
        rightMouseButton = Fe.input.mouse.addButton(MouseButton.RIGHT);
        rightMouseButton.onDown.push(function (x, y) {
            trace("Right Down:", x, y);
        });
        rightMouseButton.onUp.push(function (x, y) {
            trace("Right Up:", x, y);
        });
#end
#if mobile
        Fe.input.touches.onDown.push(function (x, y) {
            trace("Touch Down:", x, y, "Total Touches:",
                Fe.input.touches.length);
            sound.play();
        });
        Fe.input.touches.onUp.push(function (x, y) {
            trace("Touch Up:", x, y, "Total Touches:",
                Fe.input.touches.length);
        });
#end

        var switchSceneKey:Key = Fe.input.keyboard.addKey(Key.A);
        switchSceneKey.onDown.push(function () {
#if (desktop || web)
            leftMouseButton.onDown = [];
            middleMouseButton.onDown = [];
            rightMouseButton.onDown = [];
            Fe.input.keyboard.removeKey(Key.A);
            Fe.input.keyboard.removeKey(Key.RETURN);
            Fe.input.keyboard.removeKey(Key.SPACE);
            Fe.input.keyboard.removeKey(Key.R);
            Fe.input.keyboard.removeKey(Key.ESCAPE);
#end
            music.stop();
            Fe.scene = new OtherScene();
        });

        var soundKey:Key = Fe.input.keyboard.addKey(Key.RETURN);
        soundKey.onDown.push(function () {
            sound.play();
        });

        var debugKey:Key = Fe.input.keyboard.addKey(Key.SPACE);
        debugKey.onDown.push(function () {
            trace("FPS: " + Debug.fps.current + " | " +
                "Memory: " + Debug.memory + "MB" + " | " +
                "Peak Memory: " + Debug.peakMemory + "MB");
        });

        var restartKey:Key = Fe.input.keyboard.addKey(Key.R);
        restartKey.onDown.push(function () {
            Fe.game.restart();
        });

        var exitKey:Key = Fe.input.keyboard.addKey(Key.ESCAPE);
        exitKey.onDown.push(function () {
            Fe.game.exit();
        });

        trace("Images", [for (key in Assets.images.keys()) key]);
        trace("Sounds", [for (key in Assets.sounds.keys()) key]);
        trace("Fonts", [for (key in Assets.fonts.keys()) key]);
    }

    public override function update(delta:Float):Void
    {
        super.update(delta);

        spriteRenderer.sort();

        //*
        text.angle += 0.02;
        text.color += 2;
        text.updateGlyphs();
        //*/

        /*
#if (desktop || web)
        if (leftMouseButton.down)
        {
            trace("DOWN");
        }
#end
#if mobile
        if (Fe.input.touches.down)
        {
            for (touch in Fe.input.touches)
            {
                trace("Pos:", touch.pos.x, touch.pos.y);
                trace("Delta", touch.delta.x, touch.delta.y);
                trace("Prev", touch.prev.x, touch.prev.y);
                trace("Start", touch.start.x, touch.start.y);
            }
        }
#end
        */

        /*
        if (camera1.view.x < 0)
        {
            camera1.view.x = 0;
            cameraSpeed = -cameraSpeed;
        }
        else if (camera1.view.x > Fe.game.width)
        {
            camera1.view.x = Fe.game.width;
            cameraSpeed = -cameraSpeed;
        }
        camera1.view.x += cameraSpeed;
        //*/
    }
}
