package;

import ironFetus.Fe;
import ironFetus.Scene;
import ironFetus.assets.Assets;
import ironFetus.input.Key;

class OtherScene extends Scene
{
    public override function init():Void
    {
        backgroundColor = 0xFFFF0000;

        var switchSceneKey:Key = Fe.input.keyboard.addKey(Key.A);
        switchSceneKey.onDown.push(function () {
            Fe.input.keyboard.removeKey(Key.A);
            Fe.scene = new TestScene();
        });
    }
}
