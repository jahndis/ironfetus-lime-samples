package;

import ironFetus.Color;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Rectangle;
import ironFetus.Sprite;
import ironFetus.Vector2;


class TestAnimatedTile extends Sprite
{
    public var texRect:Rectangle;
    public var redSpeed:Int;
    public var greenSpeed:Int;
    public var blueSpeed:Int;
    public var alphaSpeed:Int;

    public override function new():Void
    {
        super();

		redSpeed = Fe.random.int(0, 10);
        greenSpeed = Fe.random.int(0, 10);
        blueSpeed = Fe.random.int(0, 10);
        alphaSpeed = Fe.random.int(0, 10);

        graphics[0] = new Graphic();

        //*
        graphics[0].pos.x = Fe.random.float(0, Fe.game.width);
        graphics[0].pos.y = Fe.random.float(0, Fe.game.height);
        graphics[0].angle = Fe.random.float(0, 6.28);
        graphics[0].scale.x = 0.5 + Fe.random.float();
        graphics[0].scale.y = 0.5 + Fe.random.float();
        //*/

        graphics[0].vertices = [
            new Vector2(-50, -50),
            new Vector2(50, -50),
            new Vector2(-50, 50),

            new Vector2(50, -50),
            new Vector2(50, 50),
            new Vector2(-50, 50)
        ];

        texRect = new Rectangle(0, 0, 0.5, 0.5);

        graphics[0].texels = [
            new Vector2(texRect.x, texRect.y),
            new Vector2(texRect.x + texRect.width, texRect.y),
            new Vector2(texRect.x, texRect.y + texRect.height),

            new Vector2(texRect.x + texRect.width, texRect.y),
            new Vector2(texRect.x + texRect.width, texRect.y + texRect.height),
            new Vector2(texRect.x, texRect.y + texRect.height)
        ];
    }

    public override function create():Void
    {
        super.create();
    }

    public override function destroy():Void
    {
        super.destroy();
    }

    public override function preUpdate(delta:Float):Void
    {
        super.preUpdate(delta);
    }

    public override function update(delta:Float):Void
    {
        super.update(delta);

        //*
        texRect.x = Fe.random.float(0, 0.5);
        texRect.y = Fe.random.float(0, 0.5);
        graphics[0].texels[0].setTo(texRect.x, texRect.y);
        graphics[0].texels[1].setTo(texRect.x + texRect.width, texRect.y);
        graphics[0].texels[2].setTo(texRect.x, texRect.y + texRect.height);
        graphics[0].texels[3].setTo(texRect.x + texRect.width, texRect.y);
        graphics[0].texels[4].setTo(texRect.x + texRect.width,
            texRect.y + texRect.height);
        graphics[0].texels[5].setTo(texRect.x, texRect.y + texRect.height);
        //*/

        if (Color.r(graphics[0].color) + redSpeed < 0 ||
            Color.r(graphics[0].color) + redSpeed > 255)
        {
            redSpeed *= -1;
        }
        if (Color.g(graphics[0].color) + greenSpeed < 0 ||
            Color.g(graphics[0].color) + greenSpeed > 255)
        {
            greenSpeed *= -1;
        }
        if (Color.b(graphics[0].color) + blueSpeed < 0 ||
            Color.b(graphics[0].color) + blueSpeed > 255)
        {
            blueSpeed *= -1;
        }
        if (Color.a(graphics[0].color) + alphaSpeed < 0 ||
            Color.a(graphics[0].color) + alphaSpeed > 255)
        {
            alphaSpeed *= -1;
        }
        graphics[0].color = Color.createRGB(
            Color.r(graphics[0].color) + redSpeed,
            Color.g(graphics[0].color) + greenSpeed,
            Color.b(graphics[0].color) + blueSpeed,
            Color.a(graphics[0].color) + alphaSpeed);
    }

    public override function postUpdate(delta:Float):Void
    {
        super.postUpdate(delta);
    }

    public override function dispose():Void
    {
        super.dispose();
    }
}
