package;

import ironFetus.Color;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Rectangle;
import ironFetus.Sprite;
import ironFetus.Vector2;


class TestTile extends Sprite
{
    //public var texRect:Rectangle;

    public override function new():Void
    {
        super();

        graphics[0] = new Graphic();

        //*
        graphics[0].pos.x = Fe.random.float(0, Fe.game.width);
        graphics[0].pos.y = Fe.random.float(0, Fe.game.height);
        graphics[0].angle = Fe.random.float(0, 6.28);
        graphics[0].scale.x = 0.5 + Fe.random.float();
        graphics[0].scale.y = 0.5 + Fe.random.float();
        //*/

		Graphic.makeQuadVertices(graphics[0], -50, -50, 50, 50);
		Graphic.makeQuadTexels(graphics[0], 0, 0, 1, 1);
    }

    public override function create():Void
    {
        super.create();
    }

    public override function destroy():Void
    {
        super.destroy();
    }

    public override function preUpdate(delta:Float):Void
    {
        super.preUpdate(delta);
    }

    public override function update(delta:Float):Void
    {
        super.update(delta);
    }

    public override function postUpdate(delta:Float):Void
    {
        super.postUpdate(delta);
    }

    public override function dispose():Void
    {
        super.dispose();
    }
}
