package;

import ironFetus.Color;
import ironFetus.Fe;
import ironFetus.Graphic;
import ironFetus.Sprite;
import ironFetus.Vector2;


class TestSprite extends Sprite
{
    public var pos:Vector2;
    public var speed:Vector2;
    public var scaleSpeed:Vector2;
    public var angleSpeed:Float;
    public var redSpeed:Int;
    public var greenSpeed:Int;
    public var blueSpeed:Int;
    public var alphaSpeed:Int;

    public override function new():Void
    {
        super();

		pos = new Vector2(Fe.random.float(0, Fe.game.width),
            Fe.random.float(0, Fe.game.height));

        speed = new Vector2(1 + Fe.random.float(0, 5), 1 +
            Fe.random.float(0, 5));
        scaleSpeed = new Vector2(Fe.random.float(0, 0.05),
            Fe.random.float(0, 0.05));
        angleSpeed = Fe.random.float(0, 0.05);
        redSpeed = Fe.random.int(0, 10);
        greenSpeed = Fe.random.int(0, 10);
        blueSpeed = Fe.random.int(0, 10);
        alphaSpeed = Fe.random.int(0, 10);

        graphics[0] = new Graphic();

        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;

        graphics[0].vertices = [
            new Vector2(-50, -50),
            new Vector2(50, -50),
            new Vector2(-50, 50),

            new Vector2(50, -50),
            new Vector2(50, 50),
            new Vector2(-50, 50)
        ];
        graphics[0].scale.x = 0.5 + Fe.random.float();
        graphics[0].scale.y = 0.5 + Fe.random.float();

        graphics[0].texels = [
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),

            new Vector2(1, 0),
            new Vector2(1, 1),
            new Vector2(0, 1)
        ];
    }

    public override function create():Void
    {
        super.create();
    }

    public override function destroy():Void
    {
        super.destroy();
    }

	public override function preUpdate(delta:Float):Void
    {
        super.preUpdate(delta);
    }

	public override function update(delta:Float):Void
	{
		super.update(delta);

		pos.x += speed.x;
        pos.y += speed.y;

        if (pos.x < 0 || pos.x > Fe.game.width)
        {
            speed.x *= -1;
        }
        if (pos.y < 0 || pos.y > Fe.game.height)
        {
            speed.y *= -1;
        }

        graphics[0].pos.x = pos.x;
        graphics[0].pos.y = pos.y;

        graphics[0].angle += angleSpeed;

        //*
        graphics[0].scale.x += scaleSpeed.x;
        graphics[0].scale.y += scaleSpeed.y;

        if (graphics[0].scale.x < 0.5 || graphics[0].scale.x > 1.5)
        {
            scaleSpeed.x *= -1;
        }
        if (graphics[0].scale.y < 0.5 || graphics[0].scale.y > 1.5)
        {
            scaleSpeed.y *= -1;
        }
        //*/

        if (Color.r(graphics[0].color) + redSpeed < 0 ||
            Color.r(graphics[0].color) + redSpeed > 255)
        {
            redSpeed *= -1;
        }
        if (Color.g(graphics[0].color) + greenSpeed < 0 ||
            Color.g(graphics[0].color) + greenSpeed > 255)
        {
            greenSpeed *= -1;
        }
        if (Color.b(graphics[0].color) + blueSpeed < 0 ||
            Color.b(graphics[0].color) + blueSpeed > 255)
        {
            blueSpeed *= -1;
        }
        if (Color.a(graphics[0].color) + alphaSpeed < 0 ||
            Color.a(graphics[0].color) + alphaSpeed > 255)
        {
            alphaSpeed *= -1;
        }
        graphics[0].color = Color.createRGB(
            Color.r(graphics[0].color) + redSpeed,
            Color.g(graphics[0].color) + greenSpeed,
            Color.b(graphics[0].color) + blueSpeed,
            Color.a(graphics[0].color) + alphaSpeed);
	}

	public override function postUpdate(delta:Float):Void
    {
        super.postUpdate(delta);
    }

    public override function dispose():Void
    {
        super.dispose();
    }
}
